module FinanceTypeClass where

import NewDataTypes

class Convert a where
toDouble                      :: FinanceData -> Double
isProfit                      :: FinanceData -> Bool
isEquity                      :: FinanceData -> Bool
isDividend                    :: FinanceData -> Bool
isNonCurrentAssetsNow         :: FinanceData -> Bool
isNonCurrentAssets4YearAgo    :: FinanceData -> Bool
isProfitNow                   :: FinanceData -> Bool
isProfit1YearAgo              :: FinanceData -> Bool
isProfit2YearAgo              :: FinanceData -> Bool
isProfit3YearAgo              :: FinanceData -> Bool
isPayoutRatioAverage          :: FinanceData -> Bool
isFourYearThing               :: FinanceData -> Bool 
isYearOverYear                :: FinanceData -> Bool
isExpectedPayoutRatio         :: FinanceData -> Bool
isExpectedReturnTable         :: FinanceData -> Bool
isExpectedProfit              :: FinanceData -> Bool
isExpectedDividend            :: FinanceData -> Bool
isExpectedEquity              :: FinanceData -> Bool
isHoldingYear                 :: FinanceData -> Bool
isEquityNow                   :: FinanceData -> Bool
isRoeAverage                  :: FinanceData -> Bool
isExpectedPayoutRatioAns      :: FinanceData -> Bool
isLastYearEquity              :: FinanceData -> Bool
isExpectedReturnTableAns      :: FinanceData -> Bool
isPricePerShare               :: FinanceData -> Bool
isMarketCapital               :: FinanceData -> Bool
isSharesOutstanding           :: FinanceData -> Bool
isPriceEarningsRatioEstimate  :: FinanceData -> Bool
isAssets                      :: FinanceData -> Bool
isDividendNow                 :: FinanceData -> Bool
isDividend1YearAgo            :: FinanceData -> Bool
isDividend2YearAgo            :: FinanceData -> Bool
isDividend3YearAgo            :: FinanceData -> Bool
isEquity1YearAgo              :: FinanceData -> Bool
isEquity2YearAgo              :: FinanceData -> Bool
isEquity3YearAgo              :: FinanceData -> Bool

instance Convert a => Convert [a] where
toDoubleList = map toDouble

instance Convert FinanceData where
toDouble (Profit_Now x)                    = x 
toDouble (Profit_1YearAgo x)               = x 
toDouble (Profit_2YearAgo x)               = x 
toDouble (Profit_3YearAgo x)               = x 
toDouble (Equity_Now x)                    = x 
toDouble (Equity_1YearAgo x)               = x 
toDouble (Equity_2YearAgo x)               = x 
toDouble (Equity_3YearAgo x)               = x 
toDouble (Dividend_Now x)                  = x 
toDouble (Dividend_1YearAgo x)             = x 
toDouble (Dividend_2YearAgo x)             = x 
toDouble (Dividend_3YearAgo x)             = x 
toDouble (Non_Current_Assets_Now x)        = x 
toDouble (Non_Current_Assets_4YearAgo x)   = x 
toDouble (PayoutRatioAverageAns x)         = x
toDouble (FourYearThingAns x)              = x
toDouble (HoldingYear x)                   = fromIntegral x :: Double
toDouble (RoeAverage x)                    = x
toDouble (ExpectedPayoutRatioAns x)        = x
toDouble (Price_Per_Share x)               = x
toDouble (MarketCapital x)                 = x
toDouble (Shares_Outstanding x)            = x
toDouble (Price_Earnings_Ratio_Estimate x) = x
toDouble (Assets x)                        = x
toDouble (Dividend_Now x)                  = x 
toDouble (Dividend_1YearAgo x)             = x
toDouble (Dividend_2YearAgo x)             = x
toDouble (Dividend_3YearAgo x)             = x
toDouble (Equity_1YearAgo x)               = x
toDouble (Equity_2YearAgo x)               = x
toDouble (Equity_3YearAgo x)               = x

isProfit (Profit_Now _)      = True
isProfit (Profit_1YearAgo _) = True
isProfit (Profit_2YearAgo _) = True
isProfit (Profit_3YearAgo _) = True
isProfit _                   = False

isEquity (Equity_Now _)      = True
isEquity (Equity_1YearAgo _) = True
isEquity (Equity_2YearAgo _) = True
isEquity (Equity_3YearAgo _) = True
isEquity _                   = False

isDividend (Dividend_Now _)      = True
isDividend (Dividend_1YearAgo _) = True
isDividend (Dividend_2YearAgo _) = True
isDividend (Dividend_3YearAgo _) = True
isDividend _                     = False

isNonCurrentAssetsNow (Non_Current_Assets_Now _)           = True
isNonCurrentAssetsNow _                                    = False
isNonCurrentAssets4YearAgo (Non_Current_Assets_4YearAgo _) = True
isNonCurrentAssets4YearAgo _                               = False
isProfitNow (Profit_Now _)                                 = True
isProfitNow _                                              = False
isProfit1YearAgo (Profit_1YearAgo _)                       = True
isProfit1YearAgo _                                         = False
isProfit2YearAgo (Profit_2YearAgo _)                       = True
isProfit2YearAgo _                                         = False
isProfit3YearAgo (Profit_3YearAgo _)                       = True
isProfit3YearAgo _                                         = False
isPayoutRatioAverage (PayoutRatioAverage _ _)              = True
isPayoutRatioAverage _                                     = False
isFourYearThing (FourYearThing _ _ _ _ _ _)                = True
isFourYearThing _                                          = False
isYearOverYear (YearOverYear _ _)                          = True
isYearOverYear _                                           = False
isExpectedPayoutRatio (ExpectedPayoutRatio _ _)            = True
isExpectedPayoutRatio _                                    = False
isExpectedReturnTable (ExpectedReturnTable _ _ _ _)        = True
isExpectedReturnTable _                                    = False
isExpectedProfit (ExpectedProfit _ _)                      = True
isExpectedProfit _                                         = False
isExpectedDividend (ExpectedDividend _ _)                  = True
isExpectedDividend _                                       = False
isExpectedEquity (ExpectedEquity _ _ _)                    = True
isExpectedEquity _                                         = False

isPayoutRatioAverageAns (PayoutRatioAverageAns _)          = True
isPayoutRatioAverageAns _                                  = False
isFourYearThingAns (FourYearThingAns _)                    = True
isFourYearThingAns _                                       = False

isHoldingYear (HoldingYear _)                                   = True
isHoldingYear _                                                 = False
isEquityNow (Equity_Now _)                                      = True
isEquityNow _                                                   = False
isRoeAverage (RoeAverage _)                                     = True
isRoeAverage _                                                  = False
isExpectedPayoutRatioAns (ExpectedPayoutRatioAns _)             = True
isExpectedPayoutRatioAns _                                      = False
isLastYearEquity (Equity_1YearAgo _)                            = True
isLastYearEquity _                                              = False
isExpectedReturnTableAns (ExpectedReturnTableAns _)             = True
isExpectedReturnTableAns _                                      = False
isPricePerShare (Price_Per_Share _)                             = True
isPricePerShare _                                               = False
isMarketCapital (MarketCapital _)                               = True
isMarketCapital _                                               = False
isSharesOutstanding (Shares_Outstanding _)                      = True
isSharesOutstanding _                                           = False
isPriceEarningsRatioEstimate (Price_Earnings_Ratio_Estimate _)  = True
isPriceEarningsRatioEstimate _                                  = False
isAssets (Assets _)                                             = True
isAssets _                                                      = False
isDividendNow (Dividend_Now _)                                  = True
isDividendNow _                                                 = False
isDividend1YearAgo (Dividend_1YearAgo _)                        = True
isDividend1YearAgo _                                            = False
isDividend2YearAgo (Dividend_2YearAgo _)                        = True  
isDividend2YearAgo _                                            = False  
isDividend3YearAgo (Dividend_3YearAgo _)                        = True
isDividend3YearAgo _                                            = False
isEquity1YearAgo (Equity_1YearAgo _)                            = True
isEquity1YearAgo _                                              = False  
isEquity2YearAgo (Equity_2YearAgo _)                            = True
isEquity2YearAgo _                                              = False
isEquity3YearAgo (Equity_3YearAgo _)                            = True
isEquity3YearAgo _                                              = False
