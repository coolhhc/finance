module NewDataTypes where


-- contain Raw and MetaData
data FinanceData = Price_Per_Share Price_Per_Share
                 | Assets Assets
                 | Shares_Outstanding Shares_Outstanding
                 | Price_Earnings_Ratio_Estimate Price_Earnings_Ratio_Estimate

                 | Non_Current_Assets_Now Non_Current_Assets_Now
                 | Non_Current_Assets_4YearAgo Non_Current_Assets_4YearAgo
                 | Profit_Now Profit_Now
                 | Profit_1YearAgo Profit_1YearAgo
                 | Profit_2YearAgo Profit_2YearAgo
                 | Profit_3YearAgo Profit_3YearAgo
                 | Dividend_Now Dividend_Now
                 | Dividend_1YearAgo Dividend_1YearAgo
                 | Dividend_2YearAgo Dividend_2YearAgo
                 | Dividend_3YearAgo Dividend_3YearAgo
                 | Equity_Now Equity_Now
                 | Equity_1YearAgo Equity_1YearAgo
                 | Equity_2YearAgo Equity_2YearAgo
                 | Equity_3YearAgo Equity_3YearAgo

                 | HoldingYear HoldingYear
                 | RoeAverage RoeAverage
                 | LastYearEquity LastYearEquity
                 | MarketCapital MarketCapital

                 | PayoutRatioAverage [Dividend] [Profit]
                 | FourYearThing Non_Current_Assets_Now 
                                 Non_Current_Assets_4YearAgo 
                                 Profit_Now 
                                 Profit_1YearAgo 
                                 Profit_2YearAgo 
                                 Profit_3YearAgo
                 | YearOverYear Profit_Now Profit_1YearAgo

                 | ExpectedPayoutRatio PayoutRatioAverage FourYearThing
                 | ExpectedReturnTable HoldingYear Equity_Now RoeAverage ExpectedPayoutRatio
                 | ExpectedProfit RoeAverage LastYearEquity
                 | ExpectedDividend ExpectedPayoutRatio ExpectedProfit
                 | ExpectedEquity LastYearEquity ExpectedProfit ExpectedDividend 
-----------------------------------------------------------------------------------------------------------
-- Surely there's a better way to do this -----------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------
                 | PayoutRatioAverageAns PayoutRatioAverage
                 | FourYearThingAns FourYearThing 
                 | YearOverYearAns YearOverYear
                 | ExpectedPayoutRatioAns ExpectedPayoutRatio
                 | ExpectedReturnTableAns FullTable
                 | ExpectedProfitAns ExpectedProfit
                 | ExpectedDividendAns ExpectedDividend
                 | ExpectedEquityAns ExpectedEquity 
                 | ExpectedReturnAns ExpectedReturn

                 | NotParsed 
  deriving (Show, Eq)

data FullTable = FullTable {
  equityList   ::  [Equity],
  profitList   ::  [Profit],
  dividendList ::  [Dividend]
} deriving (Show, Eq)


type Price_Per_Share = Double
type Assets = Double
type Shares_Outstanding = Double
type Price_Earnings_Ratio_Estimate = Double

type Non_Current_Assets_Now = Double
type Non_Current_Assets_4YearAgo = Double
type Profit_Now = Double
type Profit_1YearAgo = Double
type Profit_2YearAgo = Double
type Profit_3YearAgo = Double
type Dividend_Now = Double
type Dividend_1YearAgo = Double
type Dividend_2YearAgo = Double
type Dividend_3YearAgo = Double
type Equity_Now = Double
type Equity_1YearAgo = Double
type Equity_2YearAgo = Double
type Equity_3YearAgo = Double

type ExpectedPayoutRatio = Double
type PayoutRatioAverage = Double
type FourYearThing = Double
type Dividend = Double
type Profit = Double
type PayoutRatio = Double
type YearOverYear = Double
type Roe = Double
type RoeAverage = Double
type Equity = Double
type HoldingYear = Int
type LastYearEquity = Double
type MarketCapital = Double

type ExpectedProfit = Double 
type ExpectedDividend = Double
type ExpectedEquity = Double
type ExpectedReturn = Double
