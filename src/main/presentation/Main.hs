module Main where

import System.Environment

-- model
import NewDataTypes
-- service
import FileService
import MakeFData

main :: IO ()
main = do
  args <- getArgs
  programName <- getProgName
  process_args programName args

process_args :: String-> [String] -> IO ()
process_args programName [] = usage programName
process_args programName (arg:args) = 
  if args == [] then do
    raw  <- processFile arg
    let financeData = makeFinanceData raw
    cleanDisplay financeData
  else
    usage programName

cleanDisplay :: [FinanceData] -> IO ()
cleanDisplay [] = return ()
cleanDisplay (fd:fds) = do
  putStrLn $ show fd
  cleanDisplay fds

usage :: String -> IO ()
usage progName = do 
  let msg = "Usage: ./" ++ progName ++ " fileName.txt"
  putStrLn msg
