module MakeFData (
  makeFinanceData
  ) where

{-# LANGUAGE TypeSynonymInstances #-}

-- model
import NewDataTypes
import FinanceTypeClass
-- service
import PhaseOne
import PhaseTwo
import PhaseThree
import PhaseFour

makeFinanceData :: [FinanceData] -> [FinanceData]
makeFinanceData financeData = build financeData
  where
    build = partFour . partThree . partTwo . partOne
