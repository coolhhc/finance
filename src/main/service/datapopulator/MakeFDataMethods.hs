module MakeFDataMethods (
   getDataType,
   getFirstDataType,
   getData,
  )where

import NewDataTypes
import FinanceTypeClass

getDataType :: (FinanceData -> Bool) -> [FinanceData] -> [Double]
getDataType func financeData = toDoubleList $ filter func financeData

getFirstDataType :: (FinanceData -> Bool) -> [FinanceData] -> Double
getFirstDataType func financeData = (toDouble . head) (filter func financeData)

getData :: (FinanceData -> Bool) -> [FinanceData] -> FinanceData
getData func financeData = let 
  ans = filter func financeData
  in if length ans == 1 then head ans else error "getData"

-- equityRatio
equityRatio :: Equity -> Assets -> Double
equityRatio equity assets = equity / assets
