module PhaseFour (
  partFour
  ) where

-- model
import NewDataTypes
import FinanceTypeClass
-- service
import MakeFDataMethods
import PhaseFourMethods

-- add to model
--     isFullTable
--     ExpectedReturnAns
--
partFour :: [FinanceData] -> [FinanceData]
partFour financeData = let 
  expectedReturnTableAns =  getData isExpectedReturnTableAns financeData
  expectedReturnVar = expectedReturn ((\ (ExpectedReturnTableAns x) -> x) expectedReturnTableAns)
                                     (getFirstDataType isMarketCapital financeData)
                                     (getFirstDataType isPriceEarningsRatioEstimate financeData)
  expectedReturnAns = ExpectedReturnAns expectedReturnVar
  in
  expectedReturnAns : financeData
