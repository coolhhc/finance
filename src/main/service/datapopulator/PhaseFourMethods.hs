module PhaseFourMethods (
  expectedReturn
  ) where


-- model
import NewDataTypes
import FinanceTypeClass
-- service
import MakeFDataMethods
-- math library
import Numeric.FAD

import Debug.Trace

-- Dividend must be in order on time line
-- now -> future
expectedReturn :: FullTable -> MarketCapital -> Price_Earnings_Ratio_Estimate -> ExpectedReturn
expectedReturn table marketCapital priceEaringsRatioEstimate = let
  dividends = dividendList table
  finalYearProfit = last (profitList table)
  years = length dividends
  firstFunc r = makeMiniFunc dividends r
  secondFunc r = firstFunc r + lastFunc finalYearProfit years r - (realToFrac marketCapital)
  ans = zeroNewton secondFunc (0.3)
  in solve (take 1000 ans)
  where
    makeMiniFunc dividends r = makeMiniFunc' dividends 1 r
    makeMiniFunc' [] _ r = 0 * r
    makeMiniFunc' (dividend:dividends) i r= (miniFunc dividend i r) + (makeMiniFunc' dividends (i+1) r)
    miniFunc dividend i r = (realToFrac dividend) / (1 + r)^i
    lastFunc finalYearProfit i r = (realToFrac (finalYearProfit * priceEaringsRatioEstimate)) / (1 + r)^i
    --solve x | trace ("solve= "++show x) False = undefined
    solve x = last x
