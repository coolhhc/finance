module PhaseOne (
  partOne
  ) where

-- model
import NewDataTypes
import FinanceTypeClass
-- service
import MakeFDataMethods
import PhaseOneMethods

partOne :: [FinanceData] -> [FinanceData]
partOne financeData = let
  roeAverage  = RoeAverage  (makeRoeAverage financeData)
  payoutRatioAverageVar  = payoutRatioAverage  (getDataType isDividend financeData) (getDataType isProfit financeData)
  fourYearThingVar       = fourYearThing       (getFirstDataType isNonCurrentAssetsNow financeData)
                                               (getFirstDataType isNonCurrentAssets4YearAgo financeData)
                                               (getFirstDataType isProfitNow financeData)
                                               (getFirstDataType isProfit1YearAgo financeData)
                                               (getFirstDataType isProfit2YearAgo financeData)
                                               (getFirstDataType isProfit3YearAgo financeData)
  yearOverYearVar        = yearOverYear        (getFirstDataType isProfitNow financeData)
                                               (getFirstDataType isProfit1YearAgo financeData)
  marketCapitalVar       = marketCapital       (getFirstDataType isPricePerShare financeData)
                                               (getFirstDataType isSharesOutstanding financeData)
  payoutRatioAverageAns  = PayoutRatioAverageAns payoutRatioAverageVar
  fourYearThingAns       = FourYearThingAns fourYearThingVar
  yearOverYearAns        = YearOverYearAns yearOverYearVar
  marketCapitalAns       = MarketCapital marketCapitalVar
  in 
  roeAverage : payoutRatioAverageAns : fourYearThingAns : yearOverYearAns : marketCapitalAns : financeData


makeRoeAverage :: [FinanceData] -> RoeAverage
makeRoeAverage fd = roeAverage roeList
  where roeList = makeRoe fd

makeRoe :: [FinanceData] -> [Roe]
makeRoe financeData = let
  profits = getDataType isProfit financeData
  equitys = getDataType isEquity financeData
  in zipWith roe profits equitys
