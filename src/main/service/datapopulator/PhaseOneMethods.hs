module PhaseOneMethods (
  roe,
  roeAverage,
  payoutRatioAverage,
  fourYearThing,
  yearOverYear,
  marketCapital
  ) where
import NewDataTypes
import FinanceTypeClass

roe :: Double -> Double -> Roe 
roe profit equity = profit / equity

roeAverage :: [Roe] -> RoeAverage
roeAverage list@(roe:roes) = let 
  total = foldr (+) roe roes     --may need to change to foldl'
  size = fromIntegral (length list)
  in total / size

-- payoutRatioAverage
payoutRatioAverage :: [Dividend] -> [Profit] -> PayoutRatioAverage
payoutRatioAverage dividends profits = let
  (payout:payouts) = payoutRatioAverage' dividends profits
  size = fromIntegral (length (payout:payouts))
  total = foldr (+) payout payouts
  in total / size

payoutRatioAverage' :: [Dividend] -> [Profit] -> [PayoutRatio]
payoutRatioAverage' [] [] = []
payoutRatioAverage' [] _  = error "payoutRatioAverage\'"
payoutRatioAverage' _ []  = error "payoutRatioAverage\'"
payoutRatioAverage' (dividend:dividends) (profit:profits) =
  (payoutRatio dividend profit) : payoutRatioAverage' dividends profits

payoutRatio :: Dividend -> Profit -> PayoutRatio
payoutRatio dividend profit = dividend / profit

-- fourYearThing
fourYearThing :: Non_Current_Assets_Now ->
                 Non_Current_Assets_4YearAgo ->
                 Profit_Now ->
                 Profit_1YearAgo ->
                 Profit_2YearAgo ->
                 Profit_3YearAgo ->
                 FourYearThing
fourYearThing nca4 nca1 p1 p2 p3 p4 =
  ((nca4 - nca1) / (p1 + p2 + p3 + p4))

-- yearOverYear
yearOverYear :: Profit_Now -> Profit_1YearAgo -> YearOverYear
yearOverYear profitNow profitLastYear =
  ((profitNow - profitLastYear) / profitLastYear)

-- marketCapital
marketCapital :: Price_Per_Share -> Shares_Outstanding -> MarketCapital
marketCapital pricePerShare sharesOutstanding = pricePerShare * sharesOutstanding
