module PhaseThree (
  partThree
  ) where

-- model
import NewDataTypes
import FinanceTypeClass
-- service
import MakeFDataMethods
import PhaseThreeMethods

partThree :: [FinanceData] -> [FinanceData]
partThree financeData = let
  holdingYear = 10
  expectedReturnTableVar = expectedReturnTable holdingYear
                                               (getFirstDataType isEquityNow financeData)
                                               (getFirstDataType isRoeAverage financeData)
                                               (getFirstDataType isExpectedPayoutRatioAns financeData)
  expectedReturnTableAns = ExpectedReturnTableAns expectedReturnTableVar
  in 
  expectedReturnTableAns : financeData
