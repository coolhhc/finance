module PhaseThreeMethods (
  expectedReturnTable
  ) where

import NewDataTypes
import FinanceTypeClass

-- expectedReturnTable
expectedReturnTable :: HoldingYear -> Equity_Now -> RoeAverage -> ExpectedPayoutRatio -> FullTable
expectedReturnTable holdingYear equityNow roeAverage expectedPayoutRatio = let
  (equitys, profits, dividends) = expectedReturnTable' holdingYear equityNow roeAverage expectedPayoutRatio
  --in FullTable (equityNow:equitys) (0:profits) (0:dividends)
  in FullTable equitys profits dividends

expectedReturnTable' :: HoldingYear ->
                        Equity ->
                        RoeAverage ->
                        ExpectedPayoutRatio ->
                        ([Equity],[Profit],[Dividend])
expectedReturnTable' 0 _ _ _ = ([],[],[])
expectedReturnTable' holdingYear lastYearEquity roeAverage expectedPayoutRatio = let
  (equity, profit, dividend)    = expectedReturnTable'' lastYearEquity roeAverage expectedPayoutRatio
  (equitys, profits, dividends) = expectedReturnTable' (holdingYear-1) equity roeAverage expectedPayoutRatio
  in ((equity:equitys), (profit:profits), (dividend:dividends))

expectedReturnTable'' :: Equity -> RoeAverage -> ExpectedPayoutRatio -> (Equity, Profit, Dividend)
expectedReturnTable'' lastYearEquity roeAverage expectedPayoutRatio = let
  equity = expectedEquity lastYearEquity profit dividend
  profit = expectedProfit roeAverage lastYearEquity
  dividend = expectedDividend expectedPayoutRatio profit
  in (equity, profit, dividend)

-- expectedProfit
expectedProfit :: RoeAverage -> Equity -> ExpectedProfit
expectedProfit roeAverage lastYearEquity = roeAverage*lastYearEquity

-- expectedDividend
expectedDividend :: ExpectedPayoutRatio -> ExpectedProfit -> ExpectedDividend
expectedDividend expectedPayoutRatio expectedProfit = expectedProfit * expectedPayoutRatio

-- expectedEquity
expectedEquity :: Equity -> ExpectedProfit -> ExpectedDividend -> ExpectedEquity
expectedEquity lastYearEquity expectedProfit expectedDividend = lastYearEquity + expectedProfit - expectedDividend
