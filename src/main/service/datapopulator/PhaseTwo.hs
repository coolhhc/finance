module PhaseTwo (
  partTwo
  ) where

-- model
import NewDataTypes
import FinanceTypeClass
-- service
import MakeFDataMethods
import PhaseTwoMethods

partTwo :: [FinanceData] -> [FinanceData]
partTwo financeData = let
  expectedPayoutRatioVar = expectedPayoutRatio (getFirstDataType isPayoutRatioAverageAns financeData)
                                               (getFirstDataType isFourYearThingAns financeData)
  expectedPayoutRatioAns = ExpectedPayoutRatioAns expectedPayoutRatioVar
  in 
  expectedPayoutRatioAns : financeData
