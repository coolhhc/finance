module PhaseTwoMethods (
  expectedPayoutRatio  
  ) where

import NewDataTypes
import FinanceTypeClass

-- expectedPayoutRatio
expectedPayoutRatio :: PayoutRatioAverage -> FourYearThing -> ExpectedPayoutRatio
expectedPayoutRatio payoutRatioAverage fourYearThing =
  (payoutRatioAverage + (1-fourYearThing) / 2)
