module ExtractFinanceData (
    getFinanceData
  )where

import NewDataTypes

getFinanceData :: [[String]] -> [FinanceData]
getFinanceData [] = []
getFinanceData (keyValue:keyValues) = let
  attribute = process keyValue
  in
  case attribute of
    Just att -> att : getFinanceData keyValues
    Nothing  -> getFinanceData keyValues

-- do I need it to be in a specific order
-- Should call all methods in Methods
process :: [String] -> Maybe FinanceData
process (key:value:[])
  | validate key    = Just $ changeToFinanceData key value
  | otherwise       = Nothing

validate :: String -> Bool
validate key = if list /= [] then True
               else False
  where list = filter (key==) validList

changeToFinanceData :: String -> String -> FinanceData
changeToFinanceData key value
  | key == "Price_Per_Share"               = Price_Per_Share valueD
  | key == "Assets"                        = Assets valueD
  | key == "Shares_Outstanding"            = Shares_Outstanding valueD
  | key == "Price_Earnings_Ratio_Estimate" = Price_Earnings_Ratio_Estimate valueD
  | key == "Non_Current_Assets_Now"        = Non_Current_Assets_Now valueD
  | key == "Non_Current_Assets_4YearAgo"   = Non_Current_Assets_4YearAgo valueD
  | key == "Profit_Now"                    = Profit_Now valueD
  | key == "Profit_1YearAgo"               = Profit_1YearAgo valueD
  | key == "Profit_2YearAgo"               = Profit_2YearAgo valueD
  | key == "Profit_3YearAgo"               = Profit_3YearAgo valueD
  | key == "Dividend_Now"                  = Dividend_Now valueD
  | key == "Dividend_1YearAgo"             = Dividend_1YearAgo valueD
  | key == "Dividend_2YearAgo"             = Dividend_2YearAgo valueD
  | key == "Dividend_3YearAgo"             = Dividend_3YearAgo valueD
  | key == "Equity_Now"                    = Equity_Now valueD
  | key == "Equity_1YearAgo"               = Equity_1YearAgo valueD
  | key == "Equity_2YearAgo"               = Equity_2YearAgo valueD
  | key == "Equity_3YearAgo"               = Equity_3YearAgo valueD
  | otherwise                              = error ("Data " ++ show key ++ " not supported") -- throw error
  where valueD = (read value) :: Double
  
validList = [ 
              "Price_Per_Share",
              "Assets",
              "Shares_Outstanding",
              "Price_Earnings_Ratio_Estimate",
              "Non_Current_Assets_Now",
              "Non_Current_Assets_4YearAgo",
              "Profit_Now",
              "Profit_1YearAgo",
              "Profit_2YearAgo",
              "Profit_3YearAgo",
              "Dividend_Now",
              "Dividend_1YearAgo",
              "Dividend_2YearAgo",
              "Dividend_3YearAgo",
              "Equity_Now",
              "Equity_1YearAgo",
              "Equity_2YearAgo",
              "Equity_3YearAgo", 
              "Price_Per_Share" 
             ]
{-
Assets 
Shares_Outstanding 
Price_Earnings_Ratio_Estimate
Non_Current_Assets_Now
Non_Current_Assets_4YearAgo
Profit_Now 
Profit_1YearAgo
Profit_2YearAgo
Profit_3YearAgo
Dividend_Now 
Dividend_1YearAgo
Dividend_2YearAgo
Dividend_3YearAgo 
Equity_Now 
Equity_1YearAgo
Equity_2YearAgo
Equity_3YearAgo 
Price_Per_Share 
-}
