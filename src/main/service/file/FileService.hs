module FileService (
    processFile
  )where

-- integration
import FileReader
-- service
import CSVparser (parseCSV)
import ExtractFinanceData (getFinanceData)
-- model
import NewDataTypes

processFile :: String -> IO [FinanceData]
processFile fileName = do
  fileString <- readCompanyFile fileName
  let 
    stringModel = parse fileString
    statistics  = getFinanceData stringModel
  return statistics

-- From a CSV file, get Key Value Pair.
parse :: String -> [[String]]
parse fileString = let
  model = parseCSV fileString
  in
  case model of
    (Right x) -> x
    _         -> []
