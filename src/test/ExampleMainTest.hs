import Data.Monoid (mempty)
import Test.Framework (defaultMain, defaultMainWithOpts, testGroup)
import Test.Framework.Options (TestOptions, TestOptions'(..))
import Test.Framework.Runners.Options (RunnerOptions, RunnerOptions'(..))
import Test.Framework.Providers.HUnit
import Test.Framework.Providers.QuickCheck2 (testProperty)

import Test.QuickCheck
import Test.HUnit


main = defaultMain tests

mainWithOpts = do
  let empty_test_opts = mempty :: TestOptions
  let my_test_opts = empty_test_opts {
    topt_maximum_generated_tests = Just 500
  }
  let empty_runner_opts = mempty :: RunnerOptions
  let my_runner_opts = empty_runner_opts {
    ropt_test_options = Just my_test_opts
  }
  defaultMainWithOpts tests my_runner_opts

tests = [
          testGroup "First Group" [
            testProperty "one" prop_one,
            testProperty "two" prop_two
          ],

          testGroup "Second Group" [
            testProperty "three" prop_three,
            testProperty "four" prop_four
          ]
        ]


one :: Int -> Int
one x = x + 1

prop_one x = one x == x+1
  where types = (x :: Int)


two :: Int -> Int
two x = x + 2

prop_two x = two x == x+2
  where types = (x :: Int)


three :: Int -> Int
three x = x + 3

prop_three x = three x == x+3
  where types = (x :: Int)


four :: Int -> Int
four x = x + 4

prop_four x = four x == x+4
  where types = (x :: Int)






{-

import Test.Framework
import Test.Framework.Providers.QuickCheck2
import Test.QuickCheck
import MethodsTests
import FileReaderTest

main = defaultMain tests

tests = [
          testGroup "First group - service layer"
          [
            testProperty "roe test" prop_roe
            --testProperty "percent test" prop_percent
          ],

          testGroup "Second group - integration layer"
          [
            testProperty "fileReader test" prop_filereader_dummy
          ]
        ]
-}
