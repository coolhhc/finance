import Data.Monoid (mempty)
import Test.Framework (defaultMain, defaultMainWithOpts, testGroup)
import Test.Framework.Options (TestOptions, TestOptions'(..))
import Test.Framework.Runners.Options (RunnerOptions, RunnerOptions'(..))
import Test.Framework.Providers.HUnit
import Test.Framework.Providers.QuickCheck2 (testProperty)

import Test.QuickCheck
import Test.HUnit

import PhaseOneMethodsTests
import PhaseTwoMethodsTests
import PhaseThreeMethodsTests


main = defaultMain tests

mainWithOpts = do
  let empty_test_opts = mempty :: TestOptions
  let my_test_opts = empty_test_opts {
    topt_maximum_generated_tests = Just 500
  }
  let empty_runner_opts = mempty :: RunnerOptions
  let my_runner_opts = empty_runner_opts {
    ropt_test_options = Just my_test_opts
  }
  defaultMainWithOpts tests my_runner_opts

tests = [
          testGroup "Presentation Layer" [
            testProperty "PlaceHolder" prop_placeHolder
          ],

          testGroup "Service Layer" [
            testProperty "roe test" prop_roe,
            testProperty "roeAverage test" prop_roeAverage,
            -- local pass
            -- jenkins not pass *wired*
            --testProperty "fourYearThing test" prop_fourYearThing,
            testProperty "yearOverYear test" prop_yearOverYear,
            testProperty "marketCapital test" prop_marketCapital,
            testProperty "expectedPayoutRatio test" prop_expectedPayoutRatio
            {- private functions
            testProperty "expectedProfit test" prop_expectedProfit,
            testProperty "expectedDividend test" prop_expectedDividend,
            testProperty "expectedEquity test" prop_expectedEquity
            -}
          ],

          testGroup "Integration Layer" [
            testProperty "PlaceHolder" prop_placeHolder
          ]
        ]

placeHolder :: Int -> Int
placeHolder x = x

prop_placeHolder x = (placeHolder x == x)
  where types = (x :: Int)
