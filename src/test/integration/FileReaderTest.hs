module FileReaderTest where

import FileReader
--import Test.Framework
--import Test.Framework.Providers.QuickCheck2
import Test.QuickCheck

input :: String 
input = "trying it out\nHope it's oKaY\n"

-- How to test side effect?
--prop_filereader = readCompanyFile "input.txt" >>= (\x -> x == input)
prop_filereader_dummy = True == True
