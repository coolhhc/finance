module PhaseFourMethodsTests where

import PhaseFourMethods
import Test.QuickCheck

-- private functions
{-
prop_expectedProfit roeAverage equity =
  (expectedProfit roeAverage lastYearEquity == roeAverage * lastYearEquity)
  where types = (roeAverage :: Double, lastYearEquity :: Double)
prop_expectedDividend expectedPayoutRatio expectedProfit =
  (expectedDividend expectedPayoutRatio expectedProfit == expectedPayoutRatio * expectedProfit)
  where types = (expectedPayoutRatio :: Double, expectedProfit :: Double)

prop_expectedEquity lastYearEquity expectedProfit expectedDividend =
  (expectedEquity lastYearEquity expectedEquity expectedDividend == lastYearEquity + expectedProfit - expectedDividend
  where types = (lastYearEquity :: Double, expectedEquity :: Double, expectedDividend :: Double)
-}
