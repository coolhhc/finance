module PhaseOneMethodsTests where

import PhaseOneMethods
import Test.QuickCheck

prop_roe profit equity = 
  (not (equity == 0)) ==>
  (roe profit equity == profit / equity)
  where types = (profit :: Double, equity :: Double)

prop_roeAverage roes = 
  (not (length roes == 0)) ==>
  (round (roeAverage roes) == round (sum roes / fromIntegral (length roes)))
  where types = (roes :: [Double])

--prop_payoutRatioAverage

{-
-- test pass on local but not on jenkins
prop_fourYearThing nca4 nca1 p1 p2 p3 p4 = let
  array = [ p1, p2, p3, p4 ]
  sub   = nca4 - nca1
  in 
  (not (sum array == 0)) ==>
  (fourYearThing nca4 nca1 p1 p2 p3 p4 ==  sub / sum array)
  where types = (nca4 :: Double, 
                 nca1 :: Double,
                 p1   :: Double,
                 p2   :: Double,
                 p3   :: Double,
                 p4   :: Double)
-}

prop_yearOverYear profitNow profitLastYear =
  (not (profitLastYear == 0)) ==>
  (yearOverYear profitNow profitLastYear == (profitNow - profitLastYear) / profitLastYear)
  where types = (profitNow :: Double, profitLastYear :: Double)

prop_marketCapital pricePerShare sharesOutstanding =
  (marketCapital pricePerShare sharesOutstanding == pricePerShare * sharesOutstanding)
  where types = (pricePerShare :: Double, sharesOutstanding :: Double)
