module PhaseTwoMethodsTests where

import PhaseTwoMethods
import Test.QuickCheck

prop_expectedPayoutRatio payoutRatioAverage fourYearThing =
  (payoutRatioAverage > 0 ) ==>
  (expectedPayoutRatio payoutRatioAverage fourYearThing == payoutRatioAverage + (1-fourYearThing) / 2)
  where types = (payoutRatioAverage :: Double, fourYearThing :: Double)
